---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://excalidraw.com",
	"elements": [
		{
			"id": "wxkBEfKMJJtAINIs1ycFU",
			"type": "freedraw",
			"x": -193.27272820472717,
			"y": 95.13351440429688,
			"width": 0,
			"height": 0,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1766323235,
			"version": 2,
			"versionNonce": 645600141,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1662466814846,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				]
			],
			"pressures": [
				1
			],
			"simulatePressure": false,
			"lastCommittedPoint": null
		},
		{
			"id": "ZbahGP6B3ws6Bg0On6lKM",
			"type": "freedraw",
			"x": -103.45454406738281,
			"y": -284.1392059326172,
			"width": 116.90909194946289,
			"height": 100.18182373046875,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1276908749,
			"version": 41,
			"versionNonce": 311542061,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466813827,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-4.548469543457031,
					11.903854370117188
				],
				[
					-13.774017333984375,
					29.388320922851562
				],
				[
					-25.808258056640625,
					48.57881164550781
				],
				[
					-36.55146026611328,
					65.19085693359375
				],
				[
					-47.77803421020508,
					81.15858459472656
				],
				[
					-55.94281005859375,
					92.34486389160156
				],
				[
					-61.39270210266113,
					98.68089294433594
				],
				[
					-63.45454788208008,
					100.18182373046875
				],
				[
					-60.95368576049805,
					96.13525390625
				],
				[
					-49.984657287597656,
					79.59950256347656
				],
				[
					-34.77072525024414,
					60.735443115234375
				],
				[
					-20.951072692871094,
					45.000030517578125
				],
				[
					-6.718315124511719,
					33.58418273925781
				],
				[
					6.671073913574219,
					25.017623901367188
				],
				[
					20.299583435058594,
					20.261611938476562
				],
				[
					33.464866638183594,
					19.272735595703125
				],
				[
					40.416748046875,
					20.698089599609375
				],
				[
					47.611236572265625,
					24.057647705078125
				],
				[
					52.88349914550781,
					27.0823974609375
				],
				[
					53.45454406738281,
					27.272735595703125
				],
				[
					53.45454406738281,
					27.272735595703125
				]
			],
			"pressures": [
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				53.45454406738281,
				27.272735595703125
			]
		},
		{
			"id": "Ow7OGp66P-kGBvoXi6Kdi",
			"type": "freedraw",
			"x": -109.63636016845703,
			"y": -232.1392059326172,
			"width": 12.727272033691406,
			"height": 16,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 570434147,
			"version": 21,
			"versionNonce": 23362691,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466813682,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					12.727272033691406,
					16
				],
				[
					12.727272033691406,
					16
				]
			],
			"pressures": [
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				12.727272033691406,
				16
			]
		},
		{
			"id": "lJdV013gOi-esgEh54wPV",
			"type": "freedraw",
			"x": -112.54545593261719,
			"y": -198.68466186523438,
			"width": 49.090919494628906,
			"height": 41.09092712402344,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 833562019,
			"version": 33,
			"versionNonce": 35336525,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466813380,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					37.524696350097656,
					-8.363632202148438
				],
				[
					41.003997802734375,
					-7.723297119140625
				],
				[
					40.40516662597656,
					-5.516815185546875
				],
				[
					32.54872131347656,
					3.79437255859375
				],
				[
					21.786758422851562,
					15.667800903320312
				],
				[
					10.344718933105469,
					23.761871337890625
				],
				[
					2.64105224609375,
					28.564697265625
				],
				[
					-0.09389495849609375,
					30.54547119140625
				],
				[
					-0.36362457275390625,
					31.3875732421875
				],
				[
					1.8003768920898438,
					31.272735595703125
				],
				[
					14.591545104980469,
					31.272735595703125
				],
				[
					28.811126708984375,
					31.272735595703125
				],
				[
					40.094764709472656,
					32
				],
				[
					47.7398681640625,
					32.64056396484375
				],
				[
					48.727294921875,
					32.727294921875
				],
				[
					48.727294921875,
					32.727294921875
				]
			],
			"pressures": [
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				48.727294921875,
				32.727294921875
			]
		},
		{
			"id": "ZEKd0IcovEWkBHuIuT5hl",
			"type": "freedraw",
			"x": -3.090911865234375,
			"y": -245.95738220214844,
			"width": 45.099700927734375,
			"height": 82.54544067382812,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1685112387,
			"version": 43,
			"versionNonce": 627381763,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466813380,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1.09967041015625,
					48.85870361328125
				],
				[
					-0.3636322021484375,
					61.11973571777344
				],
				[
					0.07763671875,
					67.50563049316406
				],
				[
					0.3636474609375,
					69.56584167480469
				],
				[
					0.3636474609375,
					67.74757385253906
				],
				[
					0.3636474609375,
					56.78877258300781
				],
				[
					0.7272796630859375,
					43.10302734375
				],
				[
					0.7272796630859375,
					30.436965942382812
				],
				[
					1.7455291748046875,
					20.290374755859375
				],
				[
					4.2447662353515625,
					10.498825073242188
				],
				[
					8.341400146484375,
					3.300537109375
				],
				[
					13.223220825195312,
					-1.2011566162109375
				],
				[
					20.79901123046875,
					-5.94256591796875
				],
				[
					27.448089599609375,
					-7.9999847412109375
				],
				[
					31.635238647460938,
					-7.515533447265625
				],
				[
					35.85057067871094,
					-4.513092041015625
				],
				[
					40.37220764160156,
					4.3893585205078125
				],
				[
					42.75950622558594,
					18.41876220703125
				],
				[
					44.000030517578125,
					34.33740234375
				],
				[
					43.36454772949219,
					47.98921203613281
				],
				[
					42.61448669433594,
					58.651702880859375
				],
				[
					41.13386535644531,
					67.12237548828125
				],
				[
					40.3636474609375,
					72.01295471191406
				],
				[
					40.000030517578125,
					74.18180847167969
				],
				[
					39.57380676269531,
					74.54545593261719
				],
				[
					39.272735595703125,
					74.54545593261719
				],
				[
					39.272735595703125,
					74.54545593261719
				]
			],
			"pressures": [
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				39.272735595703125,
				74.54545593261719
			]
		},
		{
			"id": "s8Rxj_WUX1szzs92Ghoyi",
			"type": "freedraw",
			"x": 8.545455932617188,
			"y": -208.1392059326172,
			"width": 27.636367797851562,
			"height": 6.1818084716796875,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 379420845,
			"version": 22,
			"versionNonce": 530677677,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466813380,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					18.00701904296875,
					-5.243560791015625
				],
				[
					22.38720703125,
					-5.81817626953125
				],
				[
					24.413619995117188,
					-6.1818084716796875
				],
				[
					25.69696044921875,
					-6.1818084716796875
				],
				[
					27.1826171875,
					-5.90826416015625
				],
				[
					27.636367797851562,
					-5.81817626953125
				],
				[
					27.636367797851562,
					-5.81817626953125
				]
			],
			"pressures": [
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				27.636367797851562,
				-5.81817626953125
			]
		},
		{
			"id": "DnS71pscKhE7vNbRo7e6s",
			"type": "freedraw",
			"x": -115.09091186523438,
			"y": -191.41192626953125,
			"width": 54.54545593261719,
			"height": 53.454559326171875,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 854789347,
			"version": 29,
			"versionNonce": 159551715,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466813532,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					28.144210815429688,
					-14.399688720703125
				],
				[
					42.45252990722656,
					-16
				],
				[
					50.94316101074219,
					-14.519912719726562
				],
				[
					54.22035217285156,
					-11.520782470703125
				],
				[
					54.54545593261719,
					-10.005355834960938
				],
				[
					52.60758972167969,
					-4.367034912109375
				],
				[
					47.22904968261719,
					6.842254638671875
				],
				[
					40.316009521484375,
					16.4271240234375
				],
				[
					33.789024353027344,
					24.477203369140625
				],
				[
					28.454879760742188,
					31.7950439453125
				],
				[
					25.28900146484375,
					36.066497802734375
				],
				[
					24.727279663085938,
					37.405914306640625
				],
				[
					24.727279663085938,
					37.454559326171875
				],
				[
					24.727279663085938,
					37.454559326171875
				]
			],
			"pressures": [
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				24.727279663085938,
				37.454559326171875
			]
		},
		{
			"id": "0hNMpwVfC8uEFY8jthrvf",
			"type": "freedraw",
			"x": 2.7272796630859375,
			"y": -239.77554321289062,
			"width": 13.090911865234375,
			"height": 83.45452880859375,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1245559267,
			"version": 23,
			"versionNonce": 672072205,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466813381,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					7.0459136962890625,
					37.774993896484375
				],
				[
					9.608261108398438,
					54.45356750488281
				],
				[
					11.020797729492188,
					66.93527221679688
				],
				[
					12.513092041015625,
					76.16949462890625
				],
				[
					12.727264404296875,
					81.26217651367188
				],
				[
					13.090911865234375,
					82.76571655273438
				],
				[
					13.090911865234375,
					83.45452880859375
				],
				[
					13.090911865234375,
					82.9090576171875
				],
				[
					13.090911865234375,
					82.9090576171875
				]
			],
			"pressures": [
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				13.090911865234375,
				82.9090576171875
			]
		},
		{
			"id": "cj0noi5uTX9ibPmQFLcBJ",
			"type": "freedraw",
			"x": 4.5454559326171875,
			"y": -222.32101440429688,
			"width": 51.272735595703125,
			"height": 90.99240112304688,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1517078957,
			"version": 32,
			"versionNonce": 1505012419,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466813230,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					15.110687255859375,
					-22.90155029296875
				],
				[
					26.584564208984375,
					-25.337677001953125
				],
				[
					31.533203125,
					-24.76165771484375
				],
				[
					37.540435791015625,
					-21.36865234375
				],
				[
					42.848968505859375,
					-11.80169677734375
				],
				[
					45.10585021972656,
					5.7234039306640625
				],
				[
					45.03440856933594,
					26.891754150390625
				],
				[
					43.79057312011719,
					44.613739013671875
				],
				[
					39.952056884765625,
					57.550445556640625
				],
				[
					34.39897155761719,
					64.58248901367188
				],
				[
					29.601608276367188,
					65.65472412109375
				],
				[
					18.458404541015625,
					62.12957763671875
				],
				[
					10.46771240234375,
					55.32489013671875
				],
				[
					7.7390594482421875,
					49.34344482421875
				],
				[
					10.183929443359375,
					42.177642822265625
				],
				[
					21.818405151367188,
					28.335464477539062
				],
				[
					38.993316650390625,
					15.947372436523438
				],
				[
					51.272735595703125,
					10.181808471679688
				],
				[
					51.272735595703125,
					10.181808471679688
				]
			],
			"pressures": [
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				51.272735595703125,
				10.181808471679688
			]
		},
		{
			"id": "k1ydH3yLSeuI6VOIWjqFT",
			"type": "freedraw",
			"x": -126.7272720336914,
			"y": -82.68466186523438,
			"width": 6.5454559326171875,
			"height": 24.727294921875,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1880888291,
			"version": 15,
			"versionNonce": 384721987,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466813092,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-5.175880432128906,
					19.225250244140625
				],
				[
					-6.5454559326171875,
					24.727294921875
				],
				[
					-6.5454559326171875,
					24.727294921875
				]
			],
			"pressures": [
				1,
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				-6.5454559326171875,
				24.727294921875
			]
		},
		{
			"id": "yrUCr_2wVjacRmhoPgCEJ",
			"type": "freedraw",
			"x": -103.45454406738281,
			"y": -91.41192626953125,
			"width": 53.81818389892578,
			"height": 97.09091186523438,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 990380173,
			"version": 20,
			"versionNonce": 1896762765,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466812955,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-4.189277648925781,
					10.654998779296875
				],
				[
					-11.592559814453125,
					28.511199951171875
				],
				[
					-19.587615966796875,
					43.538848876953125
				],
				[
					-28.33203887939453,
					59.936798095703125
				],
				[
					-36.041900634765625,
					74.70968627929688
				],
				[
					-44.93949890136719,
					86.74090576171875
				],
				[
					-51.70289993286133,
					94.65634155273438
				],
				[
					-53.81818389892578,
					97.09091186523438
				],
				[
					-53.81818389892578,
					97.09091186523438
				]
			],
			"pressures": [
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				-53.81818389892578,
				97.09091186523438
			]
		},
		{
			"id": "vTRwZYSFbZgAMk4eyTrLp",
			"type": "freedraw",
			"x": -115.81818389892578,
			"y": -43.04827880859375,
			"width": 4.7272796630859375,
			"height": 113.81817626953125,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 414171363,
			"version": 21,
			"versionNonce": 723581357,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466812799,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					1.44537353515625,
					7.9449462890625
				],
				[
					3.0636444091796875,
					29.32745361328125
				],
				[
					3.7934494018554688,
					46.46380615234375
				],
				[
					4.363639831542969,
					65.5816650390625
				],
				[
					4.7272796630859375,
					82.9571533203125
				],
				[
					4.4641571044921875,
					96.97561645507812
				],
				[
					4.363639831542969,
					107.09307861328125
				],
				[
					4,
					112.62677001953125
				],
				[
					4,
					113.81817626953125
				],
				[
					4,
					113.09091186523438
				],
				[
					4,
					113.09091186523438
				]
			],
			"pressures": [
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				4,
				113.09091186523438
			]
		},
		{
			"id": "xlKEQp-RkMh2v8LUs4f0j",
			"type": "freedraw",
			"x": -84.54545593261719,
			"y": -77.59375,
			"width": 24,
			"height": 13.090911865234375,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1752446221,
			"version": 13,
			"versionNonce": 1853917667,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466812668,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					11.178802490234375,
					-5.171875
				],
				[
					19.6026611328125,
					-9.86395263671875
				],
				[
					24,
					-13.090911865234375
				],
				[
					24,
					-13.090911865234375
				]
			],
			"pressures": [
				1,
				1,
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				24,
				-13.090911865234375
			]
		},
		{
			"id": "haBvhZM7yijof6hNMtCfn",
			"type": "freedraw",
			"x": -64.18180847167969,
			"y": -81.23007202148438,
			"width": 21.454551696777344,
			"height": 40.9090576171875,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 981455917,
			"version": 23,
			"versionNonce": 728252451,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466812530,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-7.602622985839844,
					15.1484375
				],
				[
					-13.364967346191406,
					27.093536376953125
				],
				[
					-17.444801330566406,
					33.438262939453125
				],
				[
					-19.63636016845703,
					37.1318359375
				],
				[
					-20.420974731445312,
					38.29644775390625
				],
				[
					-21.18079376220703,
					39.636322021484375
				],
				[
					-21.454551696777344,
					40.4910888671875
				],
				[
					-21.454551696777344,
					40.9090576171875
				],
				[
					-19.621246337890625,
					40.542877197265625
				],
				[
					-12.840461730957031,
					38.8255615234375
				],
				[
					-6.632865905761719,
					37.5672607421875
				],
				[
					-3.429443359375,
					37.09088134765625
				],
				[
					-1.454559326171875,
					37.09088134765625
				],
				[
					-1.454559326171875,
					38.181793212890625
				],
				[
					-1.454559326171875,
					38.181793212890625
				]
			],
			"pressures": [
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				-1.454559326171875,
				38.181793212890625
			]
		},
		{
			"id": "TZabsWTMeF7u32kqerp-2",
			"type": "freedraw",
			"x": -94,
			"y": 11.133575439453125,
			"width": 82.90908813476562,
			"height": 30.545501708984375,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1602944547,
			"version": 14,
			"versionNonce": 1274186477,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466812380,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					31.126953125,
					-9.9696044921875
				],
				[
					46.59385681152344,
					-15.51513671875
				],
				[
					60.67353820800781,
					-20.46697998046875
				],
				[
					71.67922973632812,
					-24.907989501953125
				],
				[
					79.3358154296875,
					-28.53436279296875
				],
				[
					82.90908813476562,
					-30.545501708984375
				],
				[
					82.90908813476562,
					-30.545501708984375
				]
			],
			"pressures": [
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				82.90908813476562,
				-30.545501708984375
			]
		},
		{
			"id": "3j6holMv_HG9NHlKp5Nfn",
			"type": "freedraw",
			"x": -55.090911865234375,
			"y": -25.2301025390625,
			"width": 36.11274719238281,
			"height": 90.83303833007812,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1633811213,
			"version": 24,
			"versionNonce": 600670797,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466812240,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					8.102584838867188,
					30.82147216796875
				],
				[
					13.539840698242188,
					47.93487548828125
				],
				[
					16.078445434570312,
					63.23712158203125
				],
				[
					16.3636474609375,
					75.39230346679688
				],
				[
					14.758544921875,
					80.59375
				],
				[
					10.724563598632812,
					87.04885864257812
				],
				[
					5.5812530517578125,
					90.83303833007812
				],
				[
					0.1019744873046875,
					90.22003173828125
				],
				[
					-9.82708740234375,
					81.44561767578125
				],
				[
					-16.699790954589844,
					72.86624145507812
				],
				[
					-19.749099731445312,
					65.39266967773438
				],
				[
					-19.710479736328125,
					60.292999267578125
				],
				[
					-15.999984741210938,
					54.18182373046875
				],
				[
					-8.602401733398438,
					48.2886962890625
				],
				[
					1.8707733154296875,
					43.662109375
				],
				[
					13.771041870117188,
					43.16925048828125
				],
				[
					14.909103393554688,
					43.272735595703125
				],
				[
					14.909103393554688,
					43.272735595703125
				]
			],
			"pressures": [
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				14.909103393554688,
				43.272735595703125
			]
		},
		{
			"id": "ZwWKc1UwQ2FlsAhPD4HU0",
			"type": "freedraw",
			"x": 15.454544067382812,
			"y": -27.04827880859375,
			"width": 74.18182373046875,
			"height": 107.92510986328125,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1664876237,
			"version": 33,
			"versionNonce": 1911998467,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466812108,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					45.801910400390625,
					-3.636383056640625
				],
				[
					56.029205322265625,
					-1.661041259765625
				],
				[
					66.14111328125,
					3.451416015625
				],
				[
					72.26089477539062,
					10.890167236328125
				],
				[
					74.18182373046875,
					21.370513916015625
				],
				[
					72.416748046875,
					32.435455322265625
				],
				[
					66.844970703125,
					44.82989501953125
				],
				[
					60.52862548828125,
					52.9259033203125
				],
				[
					53.800201416015625,
					60.3179931640625
				],
				[
					48.05780029296875,
					65.39678955078125
				],
				[
					44.299560546875,
					67.01602172851562
				],
				[
					43.126220703125,
					63.026763916015625
				],
				[
					41.090911865234375,
					44.911529541015625
				],
				[
					41.090911865234375,
					15.885589599609375
				],
				[
					42.97831726074219,
					-13.644683837890625
				],
				[
					44.638824462890625,
					-31.390533447265625
				],
				[
					45.715362548828125,
					-38.70343017578125
				],
				[
					46.883209228515625,
					-40.701416015625
				],
				[
					47.090911865234375,
					-40.909088134765625
				],
				[
					46.056304931640625,
					-36.421356201171875
				],
				[
					40.63734436035156,
					-13.45916748046875
				],
				[
					34.35374450683594,
					14.413177490234375
				],
				[
					29.086212158203125,
					39.2159423828125
				],
				[
					24.73138427734375,
					53.4381103515625
				],
				[
					21.753997802734375,
					59.401092529296875
				],
				[
					19.636367797851562,
					60.727264404296875
				],
				[
					17.090911865234375,
					57.45452880859375
				],
				[
					17.090911865234375,
					57.45452880859375
				]
			],
			"pressures": [
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				17.090911865234375,
				57.45452880859375
			]
		},
		{
			"id": "HldyL-BHdFVq1WSgFrmBy",
			"type": "freedraw",
			"x": 2.7272796630859375,
			"y": -7.41192626953125,
			"width": 2.909088134765625,
			"height": 13.090911865234375,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 762452589,
			"version": 6,
			"versionNonce": 1934899971,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466811948,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					2.909088134765625,
					13.090911865234375
				],
				[
					2.909088134765625,
					13.090911865234375
				]
			],
			"pressures": [
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				2.909088134765625,
				13.090911865234375
			]
		},
		{
			"id": "LA9-scOtboEK5rsv4Td_v",
			"type": "freedraw",
			"x": 126.72727966308594,
			"y": -17.59375,
			"width": 18.909088134765625,
			"height": 8.727325439453125,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 626402093,
			"version": 6,
			"versionNonce": 1844320589,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1662466811823,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					8.4730224609375,
					2.82440185546875
				],
				[
					18.909088134765625,
					8.727325439453125
				],
				[
					18.909088134765625,
					8.727325439453125
				]
			],
			"pressures": [
				1,
				1,
				1,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				18.909088134765625,
				8.727325439453125
			]
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#000000",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStrokeSharpness": "sharp",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"currentItemLinearStrokeSharpness": "round",
		"gridSize": null,
		"colorPalette": {}
	},
	"files": {}
}
```
%%