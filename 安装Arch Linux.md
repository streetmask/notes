# 1.  刻录硬盘镜像
[官网](https://archlinux.org/download/)提供磁力链接跟种子，客户端可以用[qbittorrent](https://www.qbittorrent.org/)

下载好之后，我首先尝试了[ventoy](https://www.ventoy.net/en/doc_start.html)，它的安装程序(ventoy2disk)会默认会将u盘格式化，并且默认格式为exfat。对于Windows系统，若使用legacy，则需要ntfs；若使用uefi，则需要fat32

可能是由于我用的电脑不满足Windows11的硬件标准，开机时进入bios设置有些问题，目前的解决方案是使用：设置->系统->恢复->高级启动，进入之后选择：疑难解答->uefi固件设置

几次尝试之后始终无法进入ventoy界面（记得Windows10的时候应该是很顺利），于是退回原来的方案，使用[UltralIso](https://www.ultraiso.com/)类似软件来制作启动盘

# 2.  进入live系统
在bios设置中将efi下的u盘选项移到第一个，保存后退出以进入预安装步骤

这一步需要划分一块磁盘空间（正确格式化的）给新的系统，至少包含主分区跟系统分区；为新系统下载安装必要的软件，除了Linux内核以外，一般人都需要一个网络配置程序，比如：networkmanager，来联网；如果是双系统则需要安装启动引导程序，如[grub](https://www.gnu.org/software/grub/)，[指南](https://zhuanlan.zhihu.com/p/138951848?utm_id=0)

其他具体细节参见[Arch wiki](https://wiki.archlinux.org/title/Installation_guide)

# 3.  个人配置
这一步我一般会安装代理程序、aur helper、显示服务器、显卡驱动、至少有个窗口管理器和终端
接下来安装[dmenu](http://tools.suckless.org/dmenu/)、浏览器、字体
```sh
sudo pacman -S xorg-server xorg-apps xorg-xinit

# 启动xserver时一定要启动一个窗口管理器，否则会自动退出或者没法操作
# 替换成自己使用的
echo "dwm" > ~/.xinitrc
```



